\select@language {swedish}
\contentsline {section}{\numberline {1}Inledning}{1}
\contentsline {section}{\numberline {2}Invarianter}{2}
\contentsline {subsection}{\numberline {2.1}Exempel}{2}
\contentsline {subsection}{\numberline {2.2}Anv\"andning}{3}
\contentsline {section}{\numberline {3}Daikon invariantdetektorn}{4}
\contentsline {subsection}{\numberline {3.1}Instrumentationen}{5}
\contentsline {subsection}{\numberline {3.2}Invariantdetektion}{5}
\contentsline {subsection}{\numberline {3.3}Invariantslag}{6}
\contentsline {subsection}{\numberline {3.4}Exemplet}{7}
\contentsline {section}{\numberline {4}F\"orb\"attringar f\"or Daikon och vidare l\"asning}{7}
\contentsline {section}{\numberline {5}Slutsatser}{9}
\contentsline {section}{Referenser}{9}
