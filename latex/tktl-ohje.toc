\select@language {finnish}
\contentsline {section}{\numberline {1}Johdanto}{1}
\contentsline {section}{\numberline {2}{\rmfamily B\kern -.05em\textsc {i\kern -.025em b}\kern -.08emT\kern -.1667em\lower .7ex\hbox {E}\kern -.125emX}-tyylin \texttt {tktl}\ k\"aytt\"o}{1}
\contentsline {subsection}{\numberline {2.1}Merkint\"atyypit (entry types)}{1}
\contentsline {subsection}{\numberline {2.2}Kent\"at (fields)}{4}
\contentsline {subsection}{\numberline {2.3}URLin mainitseminen}{8}
\contentsline {subsection}{\numberline {2.4}Muutama ohje kent\"an \texttt {TYPE} k\"ayt\"ost\"a}{9}
\contentsline {section}{\numberline {3}L\"ahdeluettelon tuottaminen {\rmfamily B\kern -.05em\textsc {i\kern -.025em b}\kern -.08emT\kern -.1667em\lower .7ex\hbox {E}\kern -.125emX}-ohjelmaa\newline k\"aytt\"aen}{10}
\contentsline {section}{\numberline {4}Ohjelmien k\"aytt\"o}{11}
