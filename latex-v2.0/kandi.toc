\select@language {swedish}
\contentsline {section}{\numberline {1}Inledning}{1}
\contentsline {subsection}{\numberline {1.1}Definitioner}{1}
\contentsline {subsection}{\numberline {1.2}Bakgrund - orakelproblemet}{2}
\contentsline {section}{\numberline {2}Invarianter}{3}
\contentsline {subsection}{\numberline {2.1}Applikationer av invarianter}{4}
\contentsline {subsection}{\numberline {2.2}Metamorfiska relationer}{5}
\contentsline {subsection}{\numberline {2.3}Testgenerering utav invarianter}{5}
\contentsline {section}{\numberline {3}Automatisk detektion av invarianter - Daikon}{7}
\contentsline {subsection}{\numberline {3.1}Instrumentationen}{7}
\contentsline {subsection}{\numberline {3.2}Invariantmodeller}{8}
\contentsline {subsection}{\numberline {3.3}Invariantdetektion}{9}
\contentsline {subsection}{\numberline {3.4}Exempelk\"orning av Daikon p\r a array-sum}{10}
\contentsline {section}{\numberline {4}Analys av Daikon}{12}
\contentsline {subsection}{\numberline {4.1}Tidskrav}{12}
\contentsline {subsection}{\numberline {4.2}Detekterade invarianternas korrekthet}{12}
\contentsline {section}{\numberline {5}F\"orb\"attringar f\"or Daikon och vidare l\"asning}{13}
\contentsline {section}{\numberline {6}Sammanfattning}{14}
\contentsline {section}{Referenser}{15}
