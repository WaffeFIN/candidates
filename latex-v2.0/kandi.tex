\documentclass[swedish]{tktltiki}
\usepackage{epsfig}
\usepackage{subfigure}
\usepackage{url}
\usepackage{listings}
\usepackage{tcolorbox}
%\usepackage{refcheck}
\usepackage{graphicx}
\usepackage{amsthm}
\theoremstyle{definition}
\newtheorem{example}{Exempel}[section]
%Path in Unix-like (Linux, OsX) format
%\graphicspath{ {/home/user/images/} }
\tcbuselibrary{listings,skins}
\lstdefinestyle{styleC}{
	numbers=left, 
	numberstyle=\small, 
	numbersep=19pt, 
    tabsize=4,
	language=C
}
\newtcblisting{programkod}[2][]{
    arc=0pt,
    outer arc=0pt,
    listing only, 
    listing style=styleC,
    title=#2,
    #1
}
\lstdefinestyle{stylePseudo}{
	numbers=left, 
	numberstyle=\small, 
	numbersep=19pt, 
    tabsize=4,
	language=python
}
\newtcblisting{pseudokod}[2][]{
    arc=0pt,
    outer arc=0pt,
    listing only, 
    listing style=stylePseudo,
    title=#2,
    #1
}
\begin{document}

\onehalfspacing
\title{Detektion av programinvarianter f�r automatisk testgenerering}
\author{Walter Gr�nholm}
\date{\today}
\level{Kandidatsavhandling}
\maketitle
\numberofpagesinformation{{\numberofpages\ sidor + \numberofappendixpages\ bilagor}}
\classification{\protect{\ \\
\  Theory of computation $\rightarrow$ Invariants\  \\
\  Software and its engineering $\rightarrow$ Software testing and debugging\ }}
\keywords{mjukvaratestning, invarianter, invariantdetektion, Daikon, testorakel, orakelproblemet}

\begin{abstract}
I denna avhandling presenteras invarianter, deras anv�ndning i programutveckling och en teknik f�r att h�rleda dem ifr�n programkod.
Invarianterna �r p�st�enden som alltid borde g�lla i en viss kodpunkt och har flera applikationer, fr�n automatisk testgenerering till bek�mpning emot skadliga program och korrekthetsbevis f�r algoritmer.
Invarianter kan definieras manuellt, men detta �r arbetsdrygt och felben�get. Ett alternativ �r att h�rleda dem automatiskt ifr�n ett program. Som exempel p� en invariantdetektionsteknik presenteras en invariantdetektor som kallas Daikon. Daikon har varit i st�ndig utveckling sedan 1999 och �r fortfarande av de mest k�nda invariantdetektorerna. Daikon har �ppen k�llkod och �r mycket anv�nd i forskningen inom automatisk testgenerering.

\end{abstract}
\mytableofcontents

\section{Inledning} \label{intro}

En stor del av totalt utvecklingsarbete g�r �t testrelaterade uppgifter, �ven upp till 50\% \cite{claessen00}. D�rmed �r det lockande att automera testningsprocessen, som best�r av testskapande och -k�rningar, varav testskapandet oftast tar en l�ngre tid. Ist�llet f�r att testingenj�ren sj�lv skapar testen, kan detta g�ras automatiskt med hj�lp av invariantdetektion. I denna avhandling presenteras invarianter, hur de kan detekteras fr�n existerande programkod och hur man kan generera test utav invarianterna. I avhandlingen presenteras �ven metamorfisk testning, som en �vrig testgenereringsteknik relaterad till invarianter.

Till n�st i avhandlingen definieras ett antal nyckelord och testgenereringens bakgrund. I kapitel \ref{invariants} behandlas invarianter och i kapitel \ref{daikon} introduceras en invariantdetektor kallad Daikon. I kapitel \ref{analysis} analyseras Daikons tids- och rymdkrav med mera. Kapitel \ref{further} best�r av f�rb�ttringsf�rslag p� den ursprungliga implementationen av Daikon, samt alternativa invariantdetektorer. Avhandlingen sammanfattas i kapitel \ref{summary}.

\subsection{Definitioner} \label{definitions}

En \emph{invariant} �r en slags specifikation, som beskriver hur programmet fungerar via relationer mellan variabler. En invariant �r en relation mellan variabler som kan antas alltid vara sann vid en best�md punkt i programkoden, vilken i denna avhandling kommer att refereras som \emph{invarianspunkten}. Matematiskt definierar vi en invariant som en univalent funktion $I(v_1, v_2, ..., v_n) \mapsto \{0, 1\}$ som tar in en �ndlig m�ngd variabler eller konstanter och ger ut ett sanningsv�rde (sant eller falskt). Variablerna och konstanterna beh�ver inte vara numeriska. Ett exempel p� en invariant �r f�ljande relation: $I(n) \Leftrightarrow n \geq 0$, vilket betyder att $n$ �r alltid ett positivt tal i invarianspunkten.

\emph{Mjukvarutestning} inneb�r f�rs�kring av en programvaras kvalitet genom olika metoder. Strukturen av ett mjukvarutest best�r av tre delar: uppbyggandet av testinmatningen, den testade funktionen eller metoden, och validering av den testade funktionens utmatning. Korrektheten av utmatningen, och i vissa fall �ven inmatningen, valideras med \emph{p�st�enden} (eng. \emph{assertions}) som borde g�lla efter funktionsanropet. I denna avhandling behandlas invarianter synonymt med p�st�enden g�llande mjukvarutestning p� grund av deras likhet. Det �r l�tt att konvertera invarianter till p�st�enden, vilket visas i kapitel \ref{generation}.

Mjukvarutest kan ha varierande omfattningar, allt ifr�n \emph{enhetstest}, det vill s�ga test p� enstaka enheter eller funktioner, till integrationstest och systemomfattande test. I denna avhandling syftar mjukvarutest p� enhetstest. En m�ngd mjukvarutest kallas f�r en \emph{testsvit}.

\subsection{Bakgrund - orakelproblemet} \label{background}

Inom automatisk testgenerering uppkommer det ett problem: vem eller vad best�mmer om programmets tillst�nd eller utmatning �r korrekt? Detta kallas f�r orakelproblemet, och det �terst�r delvis ol�st trots flera f�rs�k p� att l�sa det \cite{barr15}. Fast det finns s� kallade automatiska testgeneratorer kr�ver de flesta generatorerna �nnu ocks� m�nsklig �vervakning i n�gon m�n.

I kontext av testning �r ett \emph{orakel} en procedur som ber�ttar om programmets utmatning �r giltigt eller felaktigt. I vanliga fall d� man skriver mjukvarutest manuellt �r det testingenj�ren sj�lv som fungerar som orakel. Om testen �r v�lskrivna och anses testa f�r programmets korrekta operation kan testen sedan fungera som orakel f�r programmet. Naturligtvis skiftar detta bara orakelproblemet ett steg upp�t, fr�n att best�mma om programmets utmatning �r korrekt till att best�mma om mjukvarutesten �r korrekta. I denna avhandling antas det dock att testen och de andra programk�rningarna �r korrekta, och d�rmed kommer mjukvarutest och orakel behandlas synonymt, om inte annat bem�rks.

Det finns tre (huvudsakliga) s�tt att skapa orakel, enligt \cite{barr15}. Oraklar kan specifieras formellt, h�rledas fr�n programkod och exempelk�rningar eller skapas av underf�rst�dd information, s� kallade generella oraklar. En generell orakel, som namnet antyder, kan fungera som orakel f�r alla program. Oftast granskar de om programmet uppn�dde ett undantagsfall (eng. \emph{exception}) eller om det uppstod ett fel som avslutade k�rningen. Eftersom generella orakel inte kan testa djupare programlogik g�r det dem ytliga och opraktiska f�r egentlig testning. Formella specifikationer beskriver d�remot programlogiken med ett standardiserat spr�k. Spr�ket kan variera fr�n v�ldigt matematiskt och formellt till naturligt spr�k. Eftersom specifikationerna beskriver programlogiken �r det trivialt att omvandla dem till ett orakel. Dock �r specifikationerna sv�ra att skriva formellt och n�r de (specifikationerna) �ndras konstant med tiden f�rblir formellt specifierade program s�llsynta och opraktiska i verkligheten.

En mer praktisk orakelgenereringsteknik h�rleder orakel fr�n antingen korrekta k�rningar av programmet eller en f�rdigt skriven, mindre grupp av tester eller testsvit. H�rledningen av orakel fr�n sj�lva programk�rningarna kan g�ras med olika metoder \cite{barr15}. I denna text presenteras programinvarianter (kapitel \ref{invariants}) som ett exempel.

\section{Invarianter} \label{invariants}

Som det n�mndes tidigare �r en invariant en funktion $I(v_1, v_2, ..., v_n) \mapsto \{0, 1\}$ som g�ller vid en viss invariantpunkt. Invariantpunkterna kan vara var som helst i koden, men praktiskt s�tt brukar de definieras f�re och efter kallandet av en funktion. I b�rjan av en funktion kan funktionens inmatning valideras och i slutet granskas funktionens resultat. I detta kapitel betraktas m�nskligt h�rledda (annoterade) invarianter. Automatiskt h�rledda invarianter bektraktas vidare i kapitel \ref{daikon}.

Exempel av funktioner med annoterade invarianter finns i boken \cite{gries81}. Som ett konkret exempel presenteras h�r funktionen \emph{array-sum} i figur \ref{invariantcodefigure}. Sj�lva programmet �r ursprungligen i \cite{gries81} skrivet kortfattat i pseudokod. Figur 1 visar en modifierad version av detta program omvandlat till C. Funktionen utger summan �ver $n$ v�rden i f�ltet $b$.

\begin{figure}
	\centering
		\begin{programkod}[hbox]{array-sum}
int arraysum(int *b, int n) {
	int i, s;
	i = 0;
	s = 0;
	while (i != n) {
		s = s + b[i];
		i = i + 1;
	}
	
	return s;
}
		\end{programkod}
	\caption{Modifierad version av exempel 15.1.1 fr�n \cite{gries81}}
	\label{invariantcodefigure}
\end{figure}

Ett antal invarianter kan h�rledas ur koden. F�r att undvika segmentationsfel m�ste variabeln $n$ vara ett icke-negativt heltal mindre �n l�ngden av f�ltet b. D�rmed �r ett exempel av en invariant i inmatningen $0 \leq n \leq |b|$. Liknande h�rledningar kan g�ras f�r andra variabler. Gries identifierar f�ljande invarianter av \emph{array-sum} som anses vara relevanta. Loopinvarianterna �r invarianter som g�ller f�r i b�rjan av varje genomg�ng av while-loopen.

\fbox{Inmatningens invariant: $n \geq 0$}

\fbox{Utmatningens invariant: $s = \left( \sum_{j=0}^{n-1} b[j] \right)$}

\fbox{Loopinvarianter: $0 \leq i \leq n-1$ och $s = \left( \sum_{j=0}^{i-1} b[j] \right)$}

En anm�rkning �r att eftersom $n \geq 0$ �r en invariant, �r ocks� $n \geq -1$ en invariant. Den f�rsta invarianten �r dock mer exakt, s� naturligtvis utm�rker vi den ist�llet f�r den andra. Notera �ven att $n \geq 0 \Rightarrow n \geq -1$. Logiskt implicerande invarianter behandlas vidare i \ref{further}.

\subsection{Applikationer av invarianter} \label{applications}
En viktig applikation av invarianter �r som hj�lp inom mjukvaruutveckling. Om framtida utvecklingar s�ndrar eller �ndrar p� redan etablerad operation �r det m�jligt att uppt�cka felet med hj�lp av invarianterna. Ut�ver detta kan invarianterna dokumentera programmets operation. Ett exempel av detta ges i form av ett experiment fr�n \cite{ernst01}.

I experimentet skulle tv� programmerare i grupp ut�ka existerande kod. Programmet hade ingen dokumentation eller andra f�rklarningar om dess funktioner och egenskaper. Som hj�lp fick programmerarna en m�ngd automatiskt detekterade invarianter av programmet. Experimentet visade att invarianterna hade en klar inverkan p� f�rst�elsen av programlogiken. (Experimentet �r beskriven i mer detalj i \cite{ernst01}.) Invarianterna kunde vid behov fungera som en slags dokumentation f�r oklar funktion och gjorde underliggande datastrukturer klarare. Detta f�rhindrade programmerarna att g�ra ett felaktigt antagande av en illa-n�mnd variabel. 

Ut�ver mjukvaruutveckling kan invarianter anv�ndas f�r detektion av skadliga program \cite{baliga11} och f�r korrekthetsbevis av algoritmer \cite{barr15}. En �vrig invariantdetektor kallad DIDUCE\cite{hangal02} anv�nder invarianter f�r att detektera abnormaliteter i programmets operation medan programmet k�rs.

\subsection{Metamorfiska relationer} \label{metamorfic}

Metamorfiska relationer �r en s�rskild typ av invariant d�r utmatningarna av en funktion bildar ett f�rh�llande. Ett konkret exempel p� en metamorfisk relation f�s ur funktionen $sin(x)$. En implementation p� $sin(x)$ b�r f�lja matematiska identiteten $sin(x) = sin(\pi-x)$. Om $sin(1.234)$ ber�knas i ett testfall kan man validera resultatet med att granska om $sin(1.234) = sin(\pi-1.234)$. Eftersom identiteten b�r g�lla oberoende av $x$ kan det metamorfa testet d�rmed genereras automatiskt f�r varje testfall. Detta kallas f�r metamorfisk testning.

Metamorfisk testning publicerades ursprungligen i \cite{chen98}, ett �r f�re invariantdetektionstekniken (Daikon). Metamorfisk testning siktar p� att validera programmets utmatning indirekt via metamorfiska relationer. Metamorfisk testning passar ypperligt f�r situationer d�r traditionell validering av programmets utmatning �r tidskr�vande, som till exempel f�r ett program $P$ som matar ut den kortaste v�gen mellan tv� noder i en stor graf. Givet en ansluten (eng. \emph{connected}) graf $G$ och tv� noder $a, b \in G$ utger $P(G, a, b)$ den kortaste v�gen fr�n $a$ till $b$ i form av en m�ngd noder, det vill s�ga $P(G, a, b) = \{a, v_1, v_2, ..., b\} \subseteq G$. Om den utr�knade v�gen �r den kortaste, m�ste den uppfylla relationen $|P(G, a, v)| + |P(G, v, b)| \geq |P(G, a, b)|$ f�r varje $v \in G$. Oberoende av de testade noderna $a$ och $b$ kan testfallet som f�ljd ut�kas med nya genererade tester f�r summan av v�garna mellan $a$ och $v$, samt $v$ och $b$. Eftersom valet av $v$ kan g�ras automatiskt, �terst�r bara specificeringen av metamorfiska relationerna som det enda n�dv�ndiga m�nskliga arbetet f�r de genererade testen.

P� grund av att metamorfisk testning baserar sig p� utomst�ende information, som till exempel matematisk teori, �r metamorfiska relationer sv�ra att detektera programmatiskt. Dock �r det m�jligt att detektera andra invarianter automatiskt och d�rp� specifera skillt metamorfiska relationer.

\subsection{Testgenerering utav invarianter} \label{generation}

Ett simpelt s�tt att generera test utav programinvarianter �r att granska att alla invarianter h�ller b�de f�re och efter ett test. Efter att man har k�rt den testade funktionen kan man validera dess utmatning samt programmets tillst�nd med invarianterna. Ifall n�gon invariant inte h�ller �r det tecken p� att funktionen �r felaktig. Testinmatningen kan ocks� valideras med invarianterna f�r att granska att man kallar p� den testade funktionen korrekt. Efter detta beh�ver man bara generera en passande testinmatning f�r den testade funktionen. Detta �r dock inte trivialt. Testinmatningen kan genereras slumpm�ssigt med att kombinera funktioner och variabler, men detta leder ofta till ytliga test.

I \cite{pacheco05} genereras testinmatningen med en inkrementell strategi, d�r testinmatningskandidater byggs upp p� liknande s�tt som Daikon genererar invariantkandidater (kapitel \ref{detection}). Genereringen anv�nder sig av programmets funktioner och variabler, samt slumpm�ssigt valda konstanter. Dessa testinmatningskandidater skapas i rundor och kan bygga p� varandra. Detta kan visualiseras med tr�dstrukturer, se figur \ref{testgenerationtreefigure}, d�r varje nod �r en kandidat. Rundan b�rjar med skapandet av nya kandidater, vartefter de testas emot invarianterna. Om kandidaten bygger upp en testinmatning som bryter emot n�gon invariant kastas kanditaten bort. Kandidaterna som skapar en valid testinmatning kan anv�ndas till fortsatt generering. I figur \ref{testgenerationtreefigure} br�t tv� kandidater p� andra omg�ngen mot n�gon invariant, vartefter de sk�rdes bort och anv�ndes inte i f�ljande genereringsrunda. Genereringen avslutas efter ett antal rundor eller efter en viss tid. Efter detta reduceras testinmatningar s� att liknande inmatningar kombineras och minimeras. Detta undviker generering av flera funktionellt identiska test och hj�lper till att g�ra testen mera l�sbara.

\begin{figure}
	\label{testgenerationtreefigure}
	\center
	\includegraphics[scale=0.667]{testgenerationtree}
	\caption{Generering av testinmatning i \cite{pacheco05}. L�dorna representerar testinmatningar som kan kombineras eller ut�kas. Testinmatningar som bryter mot n�gon invariant sk�rs bort.}
\end{figure}

Alternativ f�r automatisk testgenerering �r Palus\cite{zhang11} och Agitator\cite{boshernitsan06} programmen. Agitator en kommersiell testgenerator som h�rstammar ifr�n Daikons invariantdetektion, som presenteras i n�sta kapitel.

\section{Automatisk detektion av invarianter - Daikon} \label{daikon}
Invarianter kan specifieras manuellt, till exempel med annoteringar i programkoden, men detta kan vara arbetsdrygt och felben�get. Det �r sv�rt att garantera att de detekterade invarianterna verkligen �r invarianter, eftersom det r�cker med ett motstridande undantagsfall f�r att motbevisa den detekterade invarianten. I detta kapitel kommer d�rmed ordet invariant att syfta p� en sannolik invariant, och ordet invariantkandidat att syfta p� en m�jlig eller icke-motbevisad invariant.

Invarianter kan detekteras med minimal hj�lp, till en stor del. Ett av de mest k�nda automatiska verktygen f�r invariantdetektion kallas Daikon. Given en liten testsvit eller en m�ngd korrekta k�rningar av ett annat program, kan Daikon anv�ndas f�r att generera en m�ngd sannolika invarianter av programmet. I detta kapitel syftar \emph{program} inte p� Daikon utan p� programmet vars invarianter man vill h�rleda.

Daikon �r tv�delad. Den ena delen, den s� kallade instrumentationen, kopplar Daikon till programkoden och skapar ett modifierat program, som kan k�ras efter�t. K�rningar av det modifierade programmet skapar en fil, d�r alla variablers v�rden sp�ras. Den andra delen �r sj�lva programlogiken som detekterar invarianterna fr�n de sp�rade variablerna. Tv�delningen g�r Daikon mer modul�rt och g�r det l�ttare att st�da nya programmeringsspr�k, eftersom den andra delen �r oberoende av programmets spr�k. Daikon st�der flera spr�k, inklusive C, Java och Perl \cite{ernst07}.

Instrumentationen beskrivs i delkapitel \ref{instrumentation} och invariantdetektionen i delkapitlen \ref{detection} och \ref{results} med hj�lp av exemplet fr�n kapitel \ref{invariants}.

\subsection{Instrumentationen} \label{instrumentation}

\begin{figure}
	\label{abstractsyntaxtreefigure}
	\center
	\includegraphics[scale=0.667]{abstractsyntaxtree}
	\caption{Ett exempel p� ett abstrakt syntax tr�d som representerar koden \mbox{\emph{var lista = nyLista(10);}}.}
\end{figure}

F�r att anv�nda Daikon ska man f�rst mata in programmets k�llkod till en s� kallad \emph{instrumenterare}. K�llkoden omvandlas f�rst till ett abstrakt syntaxtr�d (AST). Kort sagt beskriver AST (programmerings-) spr�kets uppbyggnad, och spj�lker koden till atomiska enheter, som operationer och variabelnamn. Genomg�ng av tr�det avsl�jar alla variabelnamn som �r definierade i alla olika omfattningar (eng. \emph{scopes}) av programmet. I figur \ref{abstractsyntaxtreefigure} har koden $var$ $lista = nyLista(10);$ omvandlats till ett AST. Fr�n AST �r det l�tt att s�ka upp alla variabelnamn inom ett uttryck (eller annan omfattning). Till f�ljande injekteras kod som registrerar variablernas v�rden och datatyp i alla anv�ndardefinierade invarianspunkter, som till exempel i b�rjan och slutet av varje funktion. Ifall variablernas v�rden �r icke-numeriska, konverteras de f�rst till tal eller talf�ljd. Detta g�rs f�r att inte vara lika beroende av programmets spr�k, samt f�r att �ka Daikons modularitet och att simplificera instrumenteraren.

Exempelvis om en klass $person$ inneh�ller de numeriska variablerna $age$ och $id$,
och $department$ inneh�ller listan $L$ med referenser till $person$ objekt, s� kan $L$ ers�ttas med talf�ljderna $L.age$ och $L.id$. S�ledes �r alla referenser till objekten $person$ konverterade till numerisk data.
Alla �vriga referenser till objekt, vars v�rden inte l�ses, kan bytas ut med unika identifikationsnummer. D�rmed kan alla referenser till icke-numeriska objekt konverteras till tal.

Det abstrakta syntaxtr�det omvandlas tillbaka till k�rbar kod efter alla n�dv�ndiga injektioner. Det modifierade programmet skall sedan k�ras som vanligt av anv�ndaren. Under k�rningarna registreras all variabeldata till en datafil. Datafilen anv�nds i n�sta steg f�r att detektera invarianterna.

\subsection{Invariantmodeller} \label{models}
Daikon anv�nder sig av vissa f�ruthandbest�mda invariantmodeller. En invariantmodell �r en invariant med odefinierade parametrar. 

Invariantmodeller kan definieras mellan ett antal variabler eller talf�ljd. Ut�ver de givna variablerna kan nya variabler skapas och anv�ndas f�r modellerna. Till exempel kan de nya variablerna skapas av summan och differensen mellan tv� variabler, samt av en talf�ljds maximi, minimi eller summa. De nya variablerna kan anv�ndas f�r vidaregenerering i ett begr�nsat antal omg�ngar.

Till f�ljande visas en lista av invariantmodeller som anv�nds av Daikon. En mer fullst�ndig lista p� invariantmodellerna finns i \cite{ernst01} och i Daikons dokumentation. Ut�ver dessa kan anv�ndaren sj�lv ut�ka p� Daikons kod med nya modeller. I listan �r $a$, $b$ och $c$ numeriska konstanter och $x$, $y$ och $z$ variabler. $s_1$ och $s_2$ �r talf�ljd.
\begin{itemize}
\item Ekvivalens mellan variabler, variabler och konstanter eller funktioner av variabler, t.ex. $x = odefinierat$ och $z = f(x, y)$
\item J�mf�relse mellan numeriska variabler och konstanter, t.ex. $a \leq x \leq b$
\item Lini�ra kombinationer, som $y = ax + b$ och $z = ax + by + c$
\item Medlemskap: $x \in \{a, b, c\}$ och $x \in s_1$
\item Invarianter p� talf�ljder, som talf�ljdens interval och ordning. En skild invariant kan ocks� h�lla f�r alla element i en talf�ljd.
\item Likaledes kan invarianter best�mmas mellan tv� talf�ljders element, i ordning. Exempelvis $\forall i \in \{0, ..., s_1.length\} I(s_1[i], s_2[i]) \Leftrightarrow s_1 > s_2$.
\item En talf�ljd $s_1$ kan vara en delf�ljd av $s_2$.
\item En talf�ljd $s_1$ kan vara i omv�nd ordning av $s_2$.
\end{itemize}

Daikon anv�nder sig av invariantmodeller med h�gst tre parametrar. Varje funktion kan i princip ta in s� m�nga parametrar som m�jligt, men som det visas i kapitel \ref{complexity} har detta en direkt inverkan p� invariantdetektionens tidskrav.

P� grund av Daikons tv�delning g�r det inte direkt att detektera invarianter mellan programmets egna funktioner och procedurer. Som det n�mndes i kapitel \ref{metamorfic} baserar metamorfiska relationer p� s�dana invarianter och d�rmed kan inte metamorfiska relationer detekteras med Daikon. Metamorfiska relationer kan dock detekteras automatiskt, vilket har visats i \cite{gotlieb03}.

\subsection{Invariantdetektion} \label{detection}

Invariantdetektorn tar som inmatning en datafil d�r alla relevanta variablers v�rden �r listade f�r varje invarianspunkt. Utav varje kombination av en definierad invariantmodell och variabler i datafilen, skapas det en invariantkandidat per invariantpunkt. Invariantkandidaterna antas vara �kta invarianter tills motsatsen bevisas. Efter att ha skapat invariantkandidaterna uts�tts de f�r besk�rningar, som baserar sig p� den inmatade datan. Varje kandidat testas med variabelv�rden som tas ur ett av programk�rningarna. Om kandidaten h�ller f�r de inmatade variabelv�rden h�lls kandidaten kvar f�r vidare testning. Om kandidaten inte h�ller f�r de inmatade variabelv�rden �r det inte en programinvariant och den sk�rs bort. Detta g�rs i ett antal omg�ngar, en per programk�rning.

En stor del av de genererade invariantkandidaterna �r antingen irrelevanta eller s� har de uppst�tt av ett sammantr�ffande. Om Daikon skulle mata ut varje invariantkandidat skulle den m�ngden vara enorm. F�r varje kandidat r�knas sannolikheten att den skulle ha uppst�tt av en slump. Sannolikheten ber�knas med simpla statistiska metoder. I \cite{ernst01} anv�ndes $x \neq 0$ som exempel. Givet att $x$ �r j�mnt distribuerat inom intervallet $[0...m]$ �r $P(x\neq0) = (m-1)/m = 1 - 1/m$. F�r $k$ k�rningar blir sannolikheten $P(x\neq0) = (1 - 1/m)^k$. Ju l�gre sannolikheten �r att kandidaten har uppst�tt av slump, desto s�krare kan man vara om att kandidaten �r en �kta invariant. Om denna sannolikhet underskrider en anv�ndarspecifierad konfidensniv�, utm�rks kandidaten i Daikons utmatning. I kapitel \ref{correctness} diskuteras invarianternas korrekthet vidare.

\subsection{Exempelk�rning av Daikon p� array-sum} \label{results}
\begin{figure}
	\centering
		\begin{pseudokod}[hbox]{}

Inmatningens invarianter
	N = storlek(B)
	N inom [7..13]
	B
	  Alla element >= -100
	  
Utmatningens invarianter
	N = I = orig(N) = storlek(B)
	B = orig(B)
	S = summa(B)
	N inom [7..13]
	
Loopinvarianterna
	N = storlek(B)
	S = summa(B[0..I-1])
	N inom [7..13]
	I inom [0..13]
	I <= N
	B
	  Alla element inom[-100..100]
	summa(B) inom [-556..539]
	B[0] icke-noll inom [-99..96]
	B[0..I-1]
	  Alla element inom [-100..100]
	N != B[-1]
	B[0] != B[-1]
		\end{pseudokod}
	\caption{Resultaten av Daikons operation p� exemplet i fig. \ref{invariantcodefigure}. Taget och �versatt ifr�n fig. 3 i \cite{ernst01}}
	\label{resultsfigure}
\end{figure}

F�r att �terkomma till funktionen \emph{array-sum} fr�n kapitel \ref{invariants}, anv�ndes det som test f�r Daikon \cite{ernst01}. Det tillsattes en liten testsvit f�r funktionen som bestod av slumpm�ssigt genererade testinmatningar. I figur \ref{resultsfigure} syns Daikons utmatning p� k�rningarnas resultat. I utmatningen syftar $orig(N)$ p� ursprungliga parameterv�rdet av variabeln $N$, och $B[-1]$ syftar p� det sista elementet i f�ltet $B$. I kapitel \ref{invariants} n�mndes de f�ljande invarianterna f�r array-sum: $n \geq 0$ f�r inmatningen, $s = \left( \sum_{j=0}^{n-1} b[j] \right)$ f�r utmatningen och $0 \leq i \leq n-1$, samt $s = \left( \sum_{j=0}^{i-1} b[j] \right)$ f�r while-loopen. Figur \ref{resultsfigure} visar att alla de utn�mnda invarianterna av array-sum �teruppt�cktes av Daikon. Ut�ver detta uppt�cktes en m�ngd �vriga, irrelevanta invarianter, som r�kade h�lla f�r de testade k�rningarna. Ett exempel p� en irrelevant invariant �r $B[0] \neq B[-1]$. Invarianten r�kade h�lla f�r de genererade testinmatningarna och har egentligen inget med array-sums funktion att g�ra. Invarianternas korrekthet diskuteras n�rmare i delkapitel \ref{correctness}.

\section{Analys av Daikon} \label{analysis}

Daikon �r en s� kallad brute-force algoritm. I b�rjan av algoritmen genererar Daikon alla m�jliga invariantkandidater ifr�n de inmatade variablerna och funktionerna. Detta �r en relativt simpel teknik och kan leda till en v�ldigt stor m�ngd kandidater. Till f�ljande diskuteras Daikons tidskrav samt korrektheten av de detekterade invarianterna.

\subsection{Tidskrav} \label{complexity}

Ett vanligt problem med brute-force algoritmer �r att deras h�ga tidskrav. 

I \cite{ernst01} visades det empriskt att Daikons tidskrav beror p� variabelm�ngden och antalet programk�rningar. Det exakta tidskravet f�r Daikon �r sv�rt att h�rleda men i \cite{ernst01} gavdes det dock en informell tidskrav p� ${\mathcal{O}(|v|\textsuperscript{3} \cdot T_f + |I| \cdot |K| \cdot |P|)}$. $|v|$ �r m�ngden variabler i invariantpunkternas omfattning. Eftersom Daikon anv�nder sig av invariantmodeller med h�gst tre parametrar, och varje parameter best�r av en variabel eller konstant skapas det proportionellt $|v| \cdot |v| \cdot |v| = |v|\textsuperscript{3}$ invariantkandidater. Intuitivt skulle funktioner med $p$ antal parametrar leda till proportionellt $|v|\textsuperscript{p}$ invariantkandidater vilket skulle generalisera formeln vidare. $T_f$ �r tiden som kr�vs f�r att falsifiera en invariant och �r i praktiken en minimal konstant. $|I|$ �r m�ngden �kta invarianter. $|K|$ �r m�ngden av programk�rningar vilket �r lini�rt beroende av testsvitens storlek.
$|P|$ �r m�ngden av invariantpunkter, som korrelerar med storleken av det testade programmet.

\subsection{Detekterade invarianternas korrekthet} \label{correctness}

I Daikons utmatning f�rekommer b�de falska negativa och falska positiva resultat. Falskt negativa resultat �r oundvikliga, eftersom Daikon har en �ndlig repertoar av testade invariantmodeller. Till exempel kan Daikon inte detektera metamorfiska relationer (kapitel \ref{models}). Falskt positiva resultat kan minskas med att anv�nda sig av fler programk�rningar p� olika situationer, d�r de falskt positiva invarianterna kan sk�ras bort.

F�r att minska m�ngden invarianter kan man undvika att skapa ekvivalenta eller implicerande invarianter. Typiska ekvivalenta invarianter skapas av symmetriska invariantfunktioner. Daikon skapar bara en invariantkandidat av invariantmodellen $I_{E}(x,y) \Leftrightarrow x = y$, eftersom invariantkandidaten $I_{E}(y,x) \Leftrightarrow y = x$ �r irrelevant. Liknande besk�rningar kan g�ras p� logiskt implicerande invarianter och detta diskuteras i kapitel \ref{further}.

F�r att �terkomma till kapitel \ref{results}, gav Daikon ut en invariant $B[0] \neq B[-1]$ f�r array-sum funktionen (figur \ref{invariantcodefigure}). De testade v�rden inom f�ltet $B$ var slumpm�ssigt valda heltal mellan $-100$ och $100$ inklusivt, vilket ger sannolikheten $P(B[0] \neq B[-1]) = \frac{200}{201} \approx 99.5\%$ att invarianten h�ller f�r en testk�rning. Sannolikheten att invarianten h�ller f�r 100 testk�rningar �r d� $\left(\frac{200}{201}\right)^{100} \approx 60.7\%$.

Naturligtvis beror invarianternas korrekhet starkt p� korrektheten av program- eller testk�rningarna. Om en k�rning �r felaktig kan den m�jligtvis slopa flera giltiga invarianter och d�rmed producera falskt negativa resultat.

\section{F�rb�ttringar f�r Daikon och vidare l�sning} \label{further}

I ursprungliga pappret \cite{ernst01} presenterade Ernst flera f�rb�ttringsf�rslag f�r Daikon. F�rslagen �r grupperade till f�rb�ttring av invariant relevans, accelererande av sj�lva processen och �ndringar till Daikon utmatning. Ett s�tt att �ka b�de anv�ndbarheten och snabbheten av Daikon �r att sk�ra bort irrelevanta invarianter. Det n�mndes tre s�tt hur man kan g�ra detta:
\begin{itemize}
\item Fast�n Daikon minskar p� m�ngden ekvivalenta invarianter, sk�rs inte alla logiskt implicerade invarianter bort. Om invarianten $I_1$ h�ller alltid d� invarianten $I_2$ h�ller, s� kan man sk�ra bort den mindre begr�nsande invarianten, det vill s�ga $I_2$. Ett simpelt exempel �r om $I_1(x) \Leftrightarrow x > 0$ och $I_2(x) \Leftrightarrow x > -1$ �r det klart att $(x > 0 \Rightarrow x > -1)\Leftrightarrow (I_1(x) \Rightarrow I_2(x))$ vilket g�r $I_2$ redundant. Generellt om $I_i \Rightarrow I_j$ kan $I_j$ sk�ras bort.
\item Man kan ocks� manuellt specifiera orelaterade variabler. Naturligtvis skapas inga invarianter emellan dem.
\item Det tredje s�ttet att minska invarianter �r att ignorera variabler som inte har �ndrats sedan f�reg�ende invarianspunkt. Variabler som h�lls konstanta har mindre relevans, och invarianter av dem �r s�llan till nytta f�r programmeraren.
\end{itemize}

Relevansen av de detekterade invarianterna �r definierad i \cite{ernst00} som egenskapen att "[hj�lpa] en programmerare i en programmeringsuppgift". Att definiera en invariant som hj�lpsam eller inte beror p� kontextet av koden, vilket �r sv�rt f�r automatiska invariantgeneratorer att ta i beaktan. Relevansen av de detekterade invarianterna har forskats vidare i \cite{ernst00}.

Ett annat f�rb�ttringsf�rslag �r direkt anv�ndning av k�llprogrammets datastrukturer i Daikon, ist�llet f�r att konvertera datastrukturerna till tal och talf�ljder. Om invariantdetektorn kan anv�nda k�llprogrammets datastrukturer �r det m�jligt att detektera mer sofistikerade invarianter. Problemet med detta �r att det skulle kr�va skilda invariantdetektorer f�r varje programmeringsspr�k som anv�nds av k�llprogrammet. Denna id� har ocks� forskats vidare i \cite{ernst99}.

En annan f�rb�ttring kan g�ras till utr�knandet av invariantens korrekhetssannolikhet. I kapitel \ref{correctness} ber�knades sannolikheten att invarianten $B[0] \neq B[-1]$ uppst�tts av slump som $\left(\frac{200}{201}\right)^{100} \approx 60.7\%$. Fast�n denna sannolikhet �verskrider Daikons konfidensniv� av $1\%$ utm�rks �nd� invarianten i utmatningen. Korrekheten av invariantmodellen $x \neq y$ ber�knas d� inte med en statistisk model, utan n�got mera simpelt.

Daikon har ocks� f�rb�ttrats av andra. Ett exempel �r \cite{perkins04}, som siktar p� att f�rb�ttra skalbarheten med inkrementella algoritmer. En inkrementell algoritm f�rs�ker minska m�ngden ber�kningar med att bara r�kna ut n�dv�ndiga v�rden. 

\section{Sammanfattning} \label{summary}

I denna avhandling presenterades invarianter som ett praktiskt hj�lpmedel f�r en m�ngd olika applikationer, allt fr�n programdokumentation till automatisk testgenerering. Inom programutveckling kan invarianterna anv�ndas f�r att granska funktionernas korrekthet och f�r att tydligg�ra datastrukturer och programlogik. Detta �r till nytta f�r programutvecklaren, och minskar m�jliga framtida misstag. Invariant detektion �r sv�rt, men vissa verktyg finns som underl�ttar detektionen.

Daikon �r ett verktyg som detekterar invarianter med hj�lp av en m�ngd korrekta k�rningar av ett program.
Daikon kan utvidgas vidare f�r fler programmeringsspr�k, och invarianterna kan justeras enligt eget tycke.
Daikon har dock sina nackdelar och kr�ver utveckling i hastighet och invariant variation.

Fast�n invarianter kan detekteras till stor del automatiskt, ligger �nnu valideringen av korrekta programk�rningar som programutvecklarens ansvar. Orakelproblemet �r d�rmed inte l�st, och h�gst troligen kommer att vara s� i n�rframtiden.

\nocite{*}
% yksi n�ist� tai ...
%\bibliographystyle{plain}
%\bibliographystyle{acm}
\bibliographystyle{ieeetr}
% ... tai t�m� 
%\bibliographystyle{apalike}
\bibliography{kandi_kallor}
\lastpage
\appendices
\end{document}
